package app

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/config"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/kubeclient"
	"gitlab.com/gitlab-org/ci-cd/gitlab-runner-pod-cleanup/logging"

	"golang.org/x/sync/errgroup"
	k8sClient "k8s.io/client-go/kubernetes"
)

const (
	pods           = "pods"
	podsAPIVersion = "v1"
)

type KubernetesCleaner struct {
	kubeClient *k8sClient.Clientset
	logger     logging.Logger
	cfg        config.Config
}

func NewCleaner(cfg config.Config, logger logging.Logger, userAgent string) (*KubernetesCleaner, error) {
	kubeClient, err := kubeclient.GetKubeClient(userAgent)
	if err != nil {
		return nil, fmt.Errorf("getting Kubernetes client: %w", err)
	}

	return &KubernetesCleaner{
		kubeClient: kubeClient,
		logger:     logger,
		cfg:        cfg,
	}, nil
}

func (cl *KubernetesCleaner) Clean(ctx context.Context) error {
	cl.logger.Info("Starting the cleaning process on a k8s cluster")

	g, ctx := errgroup.WithContext(ctx)

	for _, namespace := range cl.cfg.Kubernetes.Namespaces {
		namespace := namespace
		g.Go(func() error {
			manager, err := NewPodsManager(newPodsManagerOpts{
				logger:     cl.logger,
				cfg:        cl.cfg,
				namespace:  namespace,
				kubeClient: cl.kubeClient,
			})

			if err != nil {
				return err
			}

			err = manager.Clean(ctx)
			if err != nil {
				manager.logger.Error(err)
			}

			return err
		})
	}

	return g.Wait()
}
